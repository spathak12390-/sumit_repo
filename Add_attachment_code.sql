DECLARE
    l_multipart apex_web_service.t_multipart_parts;
    l_blob blob;
    l_body blob;
    l_req_body clob;
    l_response clob;
    l_mime_type varchar2(255);
    l_filename varchar2(32767);
    lco_boundary CONSTANT varchar2(30) := 'XXX';

    upload_failed_exception EXCEPTION;

BEGIN

    SELECT 
        name, mime_type, blob_content
    INTO 
        l_filename, l_mime_type, l_blob
    FROM 
        apex_application_temp_files
    WHERE 
        name = :P12_FILE;

    apex_web_service.g_request_headers(1).name  := 'Authorization';
    apex_web_service.g_request_headers(1).value := 'pk_7345047_HQUHMXK5KX4M4IP6UK0CFXLHPFBO2QQN';
    apex_web_service.g_request_headers(2).name  := 'Content-Type';
    apex_web_service.g_request_headers(2).value := 'multipart/form-data';
    apex_web_service.g_request_headers(3).name  := 'Connection';
    apex_web_service.g_request_headers(3).value := 'keep-alive';
    apex_web_service.g_request_headers(4).name  := 'Accept';
    apex_web_service.g_request_headers(4).value := '*/*';
    apex_web_service.g_request_headers(5).name  := 'Accept-Encoding';
    apex_web_service.g_request_headers(5).value := 'gzip, deflate, br';
    
    apex_web_service.append_to_multipart
    (
        p_multipart     => l_multipart,
        p_name          => 'attachment',
        p_filename      => l_filename,
        p_content_type  => l_mime_type,--;boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
        p_body_blob     => l_blob
        --p_body => APEX_WEB_SERVICE.BLOB2CLOBBASE64(l_blob)
    );

    l_body := apex_web_service.generate_request_body
    (
        p_multipart => l_multipart
    );
    
    -- create the JSON request body
    apex_json.initialize_clob_output();
    apex_json.open_object();
    apex_json.write('attachment', l_body); --l_blob
    apex_json.write('filename', 'attachment');
    apex_json.close_all();

    l_req_body := apex_json.get_clob_output();
		          apex_json.free_output();

    l_response := apex_web_service.make_rest_request
    (
    	p_url              => 'https://api.clickup.com/api/v2/task/1pz6dpy/attachment',
        p_http_method      => 'POST',
        p_body_blob        => l_body,
        --p_body => APEX_WEB_SERVICE.BLOB2CLOBBASE64(l_body),
        p_transfer_timeout => 3600
    );

    IF apex_web_service.g_status_code != 200 THEN
       RAISE upload_failed_exception;
    END IF;

END;