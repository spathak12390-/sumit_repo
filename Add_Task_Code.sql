DECLARE
    -- l_request_url CLOB;
    l_response CLOB;
    l_req_body CLOB;
    l_email_id varchar(32767);
    l_user_id number;

    upload_failed_exception exception;

BEGIN

    SELECT 
        DISTINCT mem.ID, awx.EMAIL 
    INTO 
        l_user_id, l_email_id
    FROM 
        APEX_WORKSPACE_APEX_USERS awx, LOCAL_MEMBERS_LIST mem
    WHERE 
        lower(awx.EMAIL) = lower(mem.EMAIL)
    AND awx.EMAIL LIKE '%@assore.com' AND USER_NAME = :APP_USER;

    apex_web_service.g_request_headers(1).name := 'Authorization';
    apex_web_service.g_request_headers(1).value := 'pk_7345047_HQUHMXK5KX4M4IP6UK0CFXLHPFBO2QQN';
    apex_web_service.g_request_headers(2).name := 'Content-Type';
    apex_web_service.g_request_headers(2).value := 'application/json';

    -- create the JSON request body
    apex_json.initialize_clob_output();
    apex_json.open_object();
    apex_json.write('name', 'Feedback From '||:APP_ID|| ' '||:APP_NAME|| ' ' ||'Logged By '|| :APP_USER || ' '|| 'Created on' || ' '|| to_char(current_date,'DD-MM-YYYY') );
    apex_json.write('description', :P10010_FEEDBACK);
    apex_json.write('status', 'TO DO');
    apex_json.write('priority', 4);
    apex_json.write('due_date', DATE_TO_UNIX_TIMESTAMP(systimestamp + INTERVAL '1' DAY));
    apex_json.write('due_date_time', 'false');
    apex_json.write('time_estimate', 86400000);
    apex_json.write('start_date', '');
    apex_json.write('start_date_time', 'false');
    apex_json.write('notify_all', 'true');
    apex_json.write('parent', '');
    apex_json.write('links_to', '');
    apex_json.write('check_required_custom_fields', 'true');
    apex_json.open_array('tags');
        apex_json.write('aop support');
    apex_json.close_array;
    apex_json.open_array('assignees');
        apex_json.write(l_user_id);
    apex_json.close_array;
    -- custom field logic
    apex_json.open_array('custom_fields');
        apex_json.open_object;
        apex_json.write('id', '4982945d-e638-4449-ad0a-984ad8b95d8c'); -- ID of custom field named Solution
            apex_json.open_array('value');
                apex_json.write('4020f5e8-7701-434b-a4f4-0e619eb1f1e9');
            apex_json.close_array;
        apex_json.close_object;
        apex_json.open_object;
        apex_json.write('id', '68e24b66-445c-43d2-8336-062e4037298b'); -- ID of custom field named Item Type
            apex_json.open_array('value');
                apex_json.write(:P10010_ITEM_TYPE);
            apex_json.close_array;
        apex_json.close_object;
        apex_json.open_object;
        apex_json.write('id', '9dfb97ac-c6b4-481b-ae2f-96c00d4f331b'); -- ID of custom field named Experience
            apex_json.open_array('value');
                apex_json.write(:P10010_EXPERIENCE);
            apex_json.close_array;
        apex_json.close_object;
    apex_json.close_array;
    
    apex_json.close_all();

    l_req_body := apex_json.get_clob_output();
		          apex_json.free_output();

    l_response := apex_web_service.make_rest_request
	(  
		p_url => :G_BASE_URL,
		p_http_method => 'POST',
		p_body => l_req_body
	);

    IF apex_web_service.g_status_code != 200 THEN
      RAISE upload_failed_exception;
    END IF;

END;