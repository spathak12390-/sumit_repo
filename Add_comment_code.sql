DECLARE
    -- l_request_url CLOB;
    l_response CLOB;
    l_req_body CLOB;
    l_email_id varchar(32767);
    l_user_id number;

    upload_failed_exception exception;

BEGIN

    apex_web_service.g_request_headers(1).name := 'Authorization';
    apex_web_service.g_request_headers(1).value := 'pk_7345047_HQUHMXK5KX4M4IP6UK0CFXLHPFBO2QQN';
    apex_web_service.g_request_headers(2).name := 'Content-Type';
    apex_web_service.g_request_headers(2).value := 'application/json';

    -- create the JSON request body
    apex_json.initialize_clob_output();
    apex_json.open_object();
    apex_json.write('comment_text', :P9_COMMENT);
    apex_json.write('notify_all', 'false');
    apex_json.close_all();

    l_req_body := apex_json.get_clob_output();
		          apex_json.free_output();

    l_response := apex_web_service.make_rest_request
	(  
		p_url => 'https://api.clickup.com/api/v2/task/' || :TASK_ID || '/comment',
		p_http_method => 'POST',
		p_body => l_req_body
	);

    IF apex_web_service.g_status_code != 200 THEN
      RAISE upload_failed_exception;
    END IF;

END;